/*
 * Project 1: Take 5!
 * CS352 Data Structures and Algorithms
 * SCE, University of Missouri Kansas City
 * 
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * Max Coker <mdcdh6@mail.umkc.edu>
**/
#include "take5.h"

/*
 * Card Class Default Constructor
 * Card node containing the details about a cards key components.
 * faceValue && penatyValue
**/
Card::Card(int face, int penalty) : 
	_faceVal(face), _penaltyVal(penalty) {}

Card::~Card() {}

/*
 * Card Class Helper Function - Private
 * setFaceValue
 * Sets the card's face value.
**/
void Card::setFaceVal(int face) {
	this->_faceVal = face;
}

/*
 * Card Class Helper Function - Private
 * setPenaltyVal
 * Sets the card's penalty value.
**/
void Card::setPenaltyVal(int penVal) {
	this->_penaltyVal = penVal;
}

bool Card::operator< ( const Card & rhs ) const {
	//std::cout << "Comparing card is " << getFaceVal() << " less than " << rhs.getFaceVal() << std::endl;
	return getFaceVal() < rhs.getFaceVal();
}

bool Card::operator> ( const Card & rhs ) const {
	//std::cout << "Comparing card is " << getFaceVal() << " greater than " << rhs.getFaceVal() << std::endl;
	return getFaceVal() > rhs.getFaceVal();
}

bool Card::operator== ( const Card & rhs ) const {
	//std::cout << "Comparing card is " << getFaceVal() << " equal to " << rhs.getFaceVal() << std::endl;
	return getFaceVal() == rhs.getFaceVal();
}

/*
 * Card Class Member Function - Public
 * Name: getFaceVal
 * 
 * Returns the Card's faceValue
**/
int Card::getFaceVal() const {
	return _faceVal;
}

/*
 * Card Class Member Function - Public
 * Name: getPanaltyVal
 * 
 * Returns the Card's penaltyValue
**/
int Card::getPenaltyVal() const {
	return _penaltyVal;
}

/*
 * Deck Class Default Constructor
 * 
 * Initializes a deck with 104 cards and shuffles.
**/
Deck::Deck() : _currentSize(-1) {
	newDeck();
	//printDeck();
	shuffle();
	//printDeck();
	//std::cout<< "Number of remaining cards in deck: " << d->numberRemaining() << std::endl;
}

Deck::~Deck() {
	for ( int i = 0 ; i < _fullDeckSize ; i++ ) {
		delete deck[i];
	}
}

/*
 * Deck Class Helper Function - Private
 * addCard
 * Pushes given card to the stack.
**/
void Deck::addCard(Card *newCard) {
	deck[++_currentSize] = newCard;
}

/*
 * Deck Class Function - Public
 * dealCard
 * Pops top card from the stack.
**/
Card * Deck::dealCard() {
	Card *temp = deck[_currentSize];
	_currentSize--;
	return temp;
}

/*
 * Deck Class Function - Public
 * numberRemaining
 * Returns the number of cards remaining in the deck.
**/
int Deck::numberRemaining() const {
	return _currentSize + 1 ;
}

/*
 * Deck Class Helper Function - Private
 * newDeck
 * Creates 104 new cards and pushes them onto the deck.
 * Assigns facevalues to each card from 1 to 104.
 * Assigns single penalty point value to 76 cards,
 * two penalty points to nine cards, three penalty points to ten cards,
 * five penalty points to eight cards, and seven penalty points to one card.
**/
void Deck::newDeck() {
	Card *c;
	int penaltyVal;
	for ( int i = 1; i <= _fullDeckSize ; ++i ) {
		if ( i == 53 )
			penaltyVal = 7;			
		else if ( i == (11 | 22 | 33 | 44 | 55 | 66 | 77 | 88 ) )
			penaltyVal = 5;
		else if ( i == ( 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100  ) )
			penaltyVal = 3;
		else if ( i == ( 14 | 26 | 38 | 42 | 52 | 64 | 76 | 88 | 102 ) )
			penaltyVal = 2;
		else 
			penaltyVal = 1;
		//std::cout << "Creating New Card \n" << "Face: " << i << "\tPenalty: " << penaltyVal << std::endl;
		//std::cin.get();
		c = new Card(i, penaltyVal);
		addCard( c );
	}
}

/*
 * Deck Class Helper Function - Private
 * shuffle
 * Takes the cards in the deck and shuffles them randomly.
**/
void Deck::shuffle() {
	int shuff;
	Card *temp;
	for ( int i = 0; i < _fullDeckSize ; i++ ) {
		shuff=std::rand()%_fullDeckSize;
		temp=deck[shuff];
		deck[shuff]=deck[i];
		deck[i] = temp;
	}
}

/*
 * Deck Class Function - Public
 * isEmpty
 * Returns true if deck is empty.
**/
bool Deck::isEmpty() const {
	return ( _currentSize < 0 ) ? true : false;
}

void Deck::resetSize() {
	_currentSize = _fullDeckSize - 1;
}

void Deck::resetDeck() {
	resetSize();
	shuffle();
}

void Deck::printDeck() {
	for (int i = 0; i <= _currentSize; i++) {
		std::cout << deck[i]->getFaceVal() << std::endl;
	}
}

void Deck::printDeckSize() const {
	std::cout << "Size of Deck: " << numberRemaining() << std::endl;
}

/*
 * DeckQ Class Default Constructor
 * 
 * Initializes a deck with 104 cards and shuffles.
**/
DeckQ::DeckQ() : _front(-1), _back(-1) {
	rand = Random(10);
	newDeck();
	//printDeck();
	shuffle();
	//printDeck();
	//std::cout<< "Number of remaining cards in deck: " << d->numberRemaining() << std::endl;
}

DeckQ::~DeckQ() {
	Card *temp;
	while ( _back != _front ) {
		temp = dealCard();
		delete temp;
	}
}

/*
 * Deck Class Helper Function - Private
 * addCard
 * Enqueue given card
**/
void DeckQ::addCard(Card *newCard) {
	if ( ( _back + 1 ) % _fullDeckSize == _front ) {
		std::cout << "Deck is full, can't add card" << std::endl;
		return;
	}
	_back = ( _back + 1 ) % _fullDeckSize;
	deck[_back] = newCard;
	if ( _front == -1 ) {
		_front = 0;
	}
	//std::cout << "Add  Front, Back " << _front << "," << _back << std::endl;
}

/*
 * Deck Class Function - Public
 * dealCard
 * Pops top card from the stack.
**/
Card * DeckQ::dealCard() {
	if ( _front == -1 ) {
		std::cout << "Deck is empty can't deal card." << std::endl;
		return NULL;
	}
	Card *temp = deck[_front];
	if ( _front == _back ) {
		std::cout << " Front equals back so, set -1 index " << std::endl;
		_front = -1;
		_back = -1;
	} else {
		_front = ( _front + 1 ) % _fullDeckSize;
	}
	//std::cout << "Deal Front, Back " << _front << "," << _back << std::endl;
	return temp;
}

/*
 * Deck Class Function - Public
 * numberRemaining
 * Returns the number of cards remaining in the deck.
**/
int DeckQ::numberRemaining() const {
	if ( _front == _back ) {
		return 0;
	} else {
		return abs( _front - _back ) + 1 ;
	}
}

/*
 * Deck Class Helper Function - Private
 * newDeck
 * Creates 104 new cards and pushes them onto the deck.
 * Assigns facevalues to each card from 1 to 104.
 * Assigns single penalty point value to 76 cards,
 * two penalty points to nine cards, three penalty points to ten cards,
 * five penalty points to eight cards, and seven penalty points to one card.
**/
void DeckQ::newDeck() {
	Card *c;
	int penaltyVal;
	for ( int i = 1; i <= _fullDeckSize ; ++i ) {
		if ( i == 53 )
			penaltyVal = 7;			
		else if ( i == (11 | 22 | 33 | 44 | 55 | 66 | 77 | 88 ) )
			penaltyVal = 5;
		else if ( i == ( 10 | 20 | 30 | 40 | 50 | 60 | 70 | 80 | 90 | 100  ) )
			penaltyVal = 3;
		else if ( i == ( 14 | 26 | 38 | 42 | 52 | 64 | 76 | 88 | 102 ) )
			penaltyVal = 2;
		else 
			penaltyVal = 1;
		//std::cout << "Creating New Card \n" << "Face: " << i << "\tPenalty: " << penaltyVal << std::endl;
		//std::cin.get();
		c = new Card(i, penaltyVal);
		addCard( c );
	}
}

/*
 * Deck Class Helper Function - Private
 * shuffle
 * Takes the cards in the deck and shuffles them randomly.
**/
void DeckQ::shuffle() {
	if ( isEmpty() ) {
		return;
	}
	int shuff;
	Card *temp;
	for ( int i = _front; i < _back ; i = (i + 1) % _fullDeckSize ) {
		shuff=std::rand()%_fullDeckSize;
		//shuff= rand.randomInt() % _fullDeckSize;
		//std::cout << "Random: " << shuff << std::endl;
		temp=deck[shuff];
		deck[shuff]=deck[i];
		deck[i] = temp;
	}
}

/*
 * Deck Class Function - Public
 * isEmpty
 * Returns true if deck is empty.
**/
bool DeckQ::isEmpty() const {
	return ( _front < 0 ) ? true : false;
}

void DeckQ::resetSize() {
	_front = 0;
	_back = _fullDeckSize-1;
}

void DeckQ::resetDeck() {
	resetSize();
	shuffle();
}

void DeckQ::printDeck() {
	for ( int i = _front; i != _back ; i = (i + 1) % _fullDeckSize ) {
		std::cout << deck[i]->getFaceVal() << std::endl;
	}
	std::cout << deck[_back]->getFaceVal() << std::endl;
}

void DeckQ::printDeckSize() const {
	std::cout << "Size of Deck: " << numberRemaining() << std::endl;
}

Row::Row() : 
	_totPenaltyVal(0), _topCardFaceVal(0), _numberofCards(0) {}

Row::Row(Card * c) : 
	_totPenaltyVal(0), _topCardFaceVal(0), _numberofCards(0) {
	addCard(c);
}

bool Row::operator< ( const Row & rhs ) {
	return getTopCardFaceVal() < rhs.getTopCardFaceVal();
}

bool Row::operator< ( const Player & rhs ) {
	return getTopCardFaceVal() < rhs.faceOfChosenCard();
}

bool Row::operator> ( const Player & rhs ) {
	return getTopCardFaceVal() > rhs.faceOfChosenCard();
}

bool Row::operator> ( const Row & rhs ) {
	return getTopCardFaceVal() > rhs.getTopCardFaceVal();
}

bool Row::operator== ( const Row & rhs ) {
	return getTopCardFaceVal() == rhs.getTopCardFaceVal();
}

void Row::printRowStatus() const {
	std::cout << "top face, pen total, # cards\t" << getTopCardFaceVal()
		<< ", " << getTotPenaltyVal() << ", " << getNumberofCards() << std::endl;
}

/*
 * Row Class Member Function - Public
 * Name: getTotPenaltyVal
 *
 * Returns the current Penalty value of the row
**/
int Row::getTotPenaltyVal() const {
	return _totPenaltyVal;
}

/*
 * Row Class Member Function - Public
 * Name: getNumberofCards
 *
 * Returns the current number of cards in the row
**/
int Row::getNumberofCards() const {
	return _numberofCards;
}

/*
 * Row Class Member Function - Public
 * Name: getTopCardFaceVal
 *
 * Returns the value of the card currently on the top of the row
**/
int Row::getTopCardFaceVal() const {
	return _topCardFaceVal;
}

bool Row::isFull() const {
	return ( _numberofCards >= _fullRow  ) ? true : false;
}

/*
 * Row Class Helper Function - Public
 * Name: addCard
 * Takes a card and adds its penalty value to the total penalty points,
 * increments the number of cards in the row,
 * and sets the current top card face value to that of the card added
**/
void Row::addCard(Card * c) {
	_numberofCards += 1;
	_topCardFaceVal = c->getFaceVal();
	_totPenaltyVal += c->getPenaltyVal();
}

/*
 * Row Class Helper Function - Public
 * Name: resetRow
 * takes a card
 * resets all of the row's values to zero
 * adds that new card to the row.
**/

void Row::resetRow(Card * c) {
	this->_numberofCards = 0;
	this->_topCardFaceVal = 0;
	this->_totPenaltyVal = 0;
	this->addCard(c);
}

/*
 * Row Class Helper Function - Public
 * Name: addPenalty
 * takes only a cards integer penalty value
 * adds that penalty value to the value of the row
**/
void Row::addPenalty(int addVal) {
	this->_totPenaltyVal += addVal;
}

Player::Player() : _numberOfCards(-1), _penaltyPileScore(0), _playerStrategy(Random), _isDeckSorted(false) {
}

Player::Player(int num) : 
	_numberOfCards(-1), _penaltyPileScore(0), 
	_playerStrategy(LargestToSmallest), _isDeckSorted(false), 
	_playerNumber(num) { }

Player::Player(int num, StrategyType strategy) : 
	_numberOfCards(-1), _penaltyPileScore(0), _playerStrategy(strategy), 
	_isDeckSorted(false), _playerNumber(num) { }

Player::~Player() {}

Card *Player::placeCard() {
	return playerChosenCard;
}

bool Player::operator< ( const Player & rhs ) {
	return faceOfChosenCard() < rhs.faceOfChosenCard();
	//return ( playerChosenCard->getFaceVal() < rhs.playerChosenCard->getFaceVal() ) ? true : false;
}

bool Player::operator> ( const Player & rhs ) {
	return faceOfChosenCard() > rhs.faceOfChosenCard();
	//return ( playerChosenCard->getFaceVal() > rhs.playerChosenCard->getFaceVal() ) ? true : false;
}

bool Player::operator> ( const int rhs ) {
	//std::cout << "Comparing Player to int is " << getScore() << " > " << rhs << std::endl;
	return (getScore() > rhs) ? true : false;
	//return ( playerChosenCard->getFaceVal() > rhs.playerChosenCard->getFaceVal() ) ? true : false;
}

bool Player::operator== ( const Player & rhs ) {
	return faceOfChosenCard() == rhs.faceOfChosenCard();
	//return ( playerChosenCard->getFaceVal() > rhs.playerChosenCard->getFaceVal() ) ? true : false;
}

bool Player::isEmptyHand() const {
	return ( _numberOfCards < 0 ) ? true : false;
}

int Player::getPlayerNumber() const {
	return _playerNumber;
}

int Player::numberOfCardsHeld() const{
	return _numberOfCards + 1;
}

void Player::addCard(Card *newCard) {
	hand[++_numberOfCards] = newCard;
}

int Player::faceOfChosenCard() const {
	return playerChosenCard->getFaceVal();
}

void Player::addRowPenalty(int pen) {
	_penaltyPileScore += pen;
}

/*
void Player::pickCard() {
	if ( !_isDeckSorted ) {
		sortPlayerHand( );
	}
	if ( _playerStrategy == SmartPlayer ) {
		smartPlayer();
	} else {
		playerChosenCard = hand[_numberOfCards--];
	}
}
*/

void Player::pickCard(Row** gameRows) {
	if ( !_isDeckSorted ) {
		sortPlayerHand( );
	}
	if ( ( _playerStrategy == SmartPlayerSmall ) || ( _playerStrategy == SmartPlayerLarge ) ) {
		smartPlayer(gameRows);
		playerChosenCard = hand[_numberOfCards--]; // temprary used for testing without exception, because function currently doesn't pick card
	} else {
		playerChosenCard = hand[_numberOfCards--];
	}
}

void Player::smartPlayer(Row** gameRows) {
	int numberOfRows = 4;
	std::vector<Card*> playerCards;
	std::vector<Card*> notEligible;
	std::map<Card*, Row*> eligible; // Cards smaller than all row face values
	for ( int i = 0; i < _numberOfCards; i++ ) {
		playerCards.push_back( hand[i] );
	}
	std::sort( playerCards.begin(), playerCards.end() );
	for ( int i = 0; i < _numberOfCards; i++ ) {
		this->hand[i] = playerCards.back( );
		playerCards.pop_back( );
	}

	int smallestPenaltyRowIndex = findSmallestRowPenalty(gameRows);
	associateCardWithRow(gameRows, playerCards, notEligible, eligible);

 	if ( !eligible.empty() ) {
 		for ( std::map<Card*,Row*>::iterator elig = eligible.begin() ; elig != eligible.end() ; elig++ ) {
 			std::cout << "Row eligible " << elig->second->getTopCardFaceVal() << " Pen Value: " << elig->second->getTotPenaltyVal()
 				<< " Hand Card " << elig->first->getFaceVal() << std::endl;
 		}
 		std::cout << "Eligible Cards have been found." << std::endl;
		if ( _playerStrategy == SmartPlayerSmall ) {
			//std::map<Card*,Row*>::iterator elig = eligible.begin() ;
			//playerChosenCard = &elig->first;
			;
		} else if ( _playerStrategy == SmartPlayerLarge ) {
			//std::map<Card*,Row*>::iterator elig = eligible.end() ;
			//playerChosenCard = (*elig).first;
		}
 	} else {
		//for ( std::vector<Card*>::iterator card = playerCards.begin() ; card != playerCards.end(); card++ ) {
		//	std::cout << "Card: " << (*card)->getFaceVal() << std::endl;
		//}
		//std::vector<Card*>::iterator card = playerCards.begin() ;
		//playerChosenCard = *card;

	}
	
}

void Player::associateCardWithRow(Row** gameRow, std::vector<Card*> &myHand, std::vector<Card*> &notEligible, std::map<Card*,Row*> &eligibleCards) {
	const int numberOfRows = 4;
	//std::cout << "Making the assumption that the rows are ordered smallest to largest" << std::endl;
	//for ( int i = 0; i < numberOfRows; i++ ) {
	//	std::cout << i << " Finding Eligible Cards for Row: " << gameRow[i]->getTopCardFaceVal() << std::endl;
	//}
	int rowIndex, newRI;
	rowIndex = 0;
	int cardIndex = 0;
	int diff, newDiff;
	for ( std::vector<Card*>::iterator card = myHand.begin(); card != myHand.end(); ++card ) {
		// Check if row is eligible for card :: card face > row face
		diff = (*card)->getFaceVal() - gameRow[rowIndex]->getTopCardFaceVal();
		while ( ( diff > 0 ) && ( ( rowIndex + 1 ) < numberOfRows ) ) { // If card is greater than and there are other rows to check
			newRI = rowIndex + 1;
			newDiff = (*card)->getFaceVal() - gameRow[newRI]->getTopCardFaceVal();
			if ( newDiff > 0 ) { // next row is eligible, check following row for elibility; this row is definitly eligible so change rowIndex
				rowIndex = newRI;
			}
			diff = newDiff;
		}
		if ( diff > 0 ) { // Found Eligible Card for Row
			//std::cout << "Found Eligible Card for Row" << std::endl;
			//std::cout << "Row, Card " << gameRow[newRI]->getTopCardFaceVal() << ", " << (*card)->getFaceVal() << std::endl;
			//std::cout << "Card Random Access vs Index " << (*card)->getFaceVal() << " -- " << myHand[cardIndex]->getFaceVal() << std::endl;
			//eligibleCards[ *card ] = gameRow[newRI];
			//eligibleCards[ *card ] = gameRow[newRI];
			eligibleCards[ myHand[cardIndex] ] = gameRow[newRI];
			//card = myHand.remove(card); // Remove eligible cards from hand for temp amound of time
		} else {
			notEligible.push_back( myHand[cardIndex] );
			;	//std::cout << "Card not Eligible for Row" << std::endl;
		}
		//std::cout << std::endl;
		cardIndex++;
	}
}

int Player::findSmallestRowPenalty(Row** gameRow) {
	int smallestPen = 100;
	for ( int i = 0; i < 4; i++ ) {
		if ( gameRow[i]->getTotPenaltyVal() < smallestPen ) {
			smallestPen = gameRow[i]->getTotPenaltyVal();
		}
	}
	return smallestPen;
}

int Player::checkSmallestDifference(int currDiff, int currRow, int cardFace, Row** rows)  {
	std::cout << "Entering Smallest Difference" << std::endl;
	int numberOfRows = 4;
	int newRow, newDiff;
	if ( currRow >= numberOfRows ) { return currRow; }

	newDiff = cardFace - rows[currRow]->getTopCardFaceVal();
	std::cout << "New Difference: " << newDiff << std::endl;

	if ( newDiff <= 0 ) {
		std::cout << "Next Row is greater than current card." << std::endl;
		std::cout << "Row, Card " << rows[currRow]->getTopCardFaceVal() << ", " << cardFace << std::endl;
		return currRow;
	} else if ( newDiff < currDiff ) {
		std::cout << "Next Row is less than current card, increment row index" << std::endl;
		std::cout << "Row, Card " << rows[currRow]->getTopCardFaceVal() << ", " << cardFace << std::endl;
		newRow = currRow + 1;
		return checkSmallestDifference(newDiff, newRow, cardFace, rows);
	}
}

void Player::sortPlayerHand( ) {
	if ( _playerStrategy == Random ) {
		;
	} else if ( _playerStrategy == LargestToSmallest ) {
		sortHandLargestToSmallest();
	} else { //if ( ( _playerStrategy == SmallestToLargest ) || 
		 //	( _playerStrategy == SmartPlayer) ) {
		sortHandSmallestToLargest();
	}
	_isDeckSorted = true;
}

void Player::sortHandLargestToSmallest() {
	std::vector<Card*> temp;
	for ( int i = 0; i < _numberOfCards ; i++ ) {
		temp.push_back( hand[i] );
	}
	std::sort( temp.begin(), temp.end(), isCardGreater);
	for ( int i = 0; i < temp.size(); ++i ) {
		hand[i] = temp[i];
	}
	//printPlayerHand();
	//std::cout << std::endl;
}

void Player::sortHandSmallestToLargest() {
	std::vector<Card*> temp;
	for ( int i = 0; i < _numberOfCards ; i++ ) {
		temp.push_back( hand[i] );
	}
	std::sort( temp.begin(), temp.end() );
	for ( int i = 0; i < temp.size(); ++i ) {
		hand[i] = temp[i];
	}
	//printPlayerHand();
	//std::cout << std::endl;
}

void Player::printPlayerHand() const {
	std::cout << "Player has " << _numberOfCards << std::endl;
	for ( int i = 0 ; i < _numberOfCards ; i++ ) {
		std::cout << "\tFace: " << hand[i]->getFaceVal() << std::endl;
	}
}

int Player::getScore() const {
	return _penaltyPileScore;
}

void Player::printScore() const {
	std::cout << "Player " << getPlayerNumber() << "\'s Score: " << getScore() << std::endl;
}

Game::Game(int numOfPlayers) : _numberOfPlayers(numOfPlayers), _numberOfRounds(0) {
	//for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
	//	players[i] = new Player(i);
	//}
	players[0] = new Player(0, Player::StrategyType::Random);
	players[1] = new Player(1, Player::StrategyType::SmallestToLargest);
	players[2] = new Player(2, Player::StrategyType::SmallestToLargest);
	players[3] = new Player(3, Player::StrategyType::SmartPlayerSmall);
	//players[3] = new Player(3, Player::StrategyType::Random);
	deck = new DeckQ();
	//std::cout << "Deck Address Upon Init: " << &deck << std::endl;
	deal();
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		rows[i] = new Row(deck->dealCard());
	}
	//orderRows();
	//printPlayerHands();
}

void Game::printPlayerHands() const {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		std::cout << "Player " << i+1 << " Cards" << std::endl;
		players[i]->printPlayerHand();
	}
}

Game::~Game() {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		delete players[i];
	}
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		delete rows[i];
	}
	delete deck;
}

void Game::resetDeckAndRows() {
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		delete rows[i];
	}
	delete deck;
	deck = new DeckQ();
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		rows[i] = new Row(deck->dealCard());
	}
}

void Game::deal() {
	int cardsDealt = 0;
	int numberOfCardsToDeal = 10;
	while (cardsDealt <= numberOfCardsToDeal && !deck->isEmpty()) {
		for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
			players[i]->addCard(deck->dealCard());
			//deck->printDeckSize();
		}
		cardsDealt++;
	}
}

void Game::playersPickCards() {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		//players[i]->pickCard();
		players[i]->pickCard(rows);
		//std::cout << "Player's Card: " << 
		//players[i]->faceOfChosenCard() << std::endl;
	}
}

void Game::orderPlayers() {
	int top = 0;
	int bottom = _numberOfPlayers - 1;
	//playerQuickSort(top , bottom);
	quickSort(players, top , bottom);
}
void Game::orderRows() {
	int top = 0;
	int bottom = _numberOfRows - 1;
	//rowQuickSort(top , bottom);
	//quickSort<Row>(rows, top , bottom);
	//std::cout << "****************" << std::endl;
	//printRows();
	//std::cout << "################" << std::endl;
	quickSort(rows, top , bottom);
	//printRows();
	//std::cout << "****************" << std::endl;
}

// template<class T> void Game::quickSort(T** thing, int left, int right) {
// //void Game::quickSort(T* thing, int left, int right) {
// 	int thingSize = (right - left) + 1;
// 	if( (thingSize > 1) && (left < right) ) {
// 		int middle = right;
// 		int intLeft = left;
// 		int intRight = right;
// 		while ( left != right ) {
// 			// Find left row whose face is small than middle
// 			while ( *thing[left] < *thing[middle] ) {
// 				left++;
// 			}
// 			// Find right row whose face is larger than middle
// 			while ( *thing[right] > *thing[middle] ) {
// 				right--;
// 			}
// 			if ( left == middle ) {
// 				middle = right;
// 			}
// 			else if ( right == middle ) {
// 				middle = left;
// 			}
// 			T *temp = thing[left];
// 			thing[left] = thing[right];
// 			thing[right] = temp;
// 			if ( ( *thing[left] == *thing[middle] ) || ( *thing[right] == *thing[middle] )) {
// 				break;
// 			}
// 		}
// 		quickSort(thing, intLeft, middle-1);
// 		quickSort(thing, middle+1, intRight);
// 	}
// }

// Check 1st Player to See if Hand is Empty
bool Game::playerHandsEmpty() const {
	return players[0]->isEmptyHand();
}

bool Game::endOfGame() const {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		if ( *players[i] > _maxPlayerScore ) {
			//std::cout << "Player score is greater than 94" << std::endl;
			return true;
		}
	}
	return false;
};

void Game::playRound() {
	_numberOfRounds++;
	while ( !playerHandsEmpty() ) {
		playersPickCards();
		orderPlayers();
		placePlayerCards();
		//std::cout << players[0]->numberOfCardsHeld() << std::endl;
	}
}

void Game::printPlayerScores() const {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		players[i]->printScore();
	}
}

void Game::printNumOfRounds() const {
	std::cout << "Number of Rounds Played: " << _numberOfRounds << std::endl;
}

void Game::playGame() {
	//printPlayerScores();
	while ( !endOfGame() && !deck->isEmpty()) {
		orderRows();
		playRound();
		//printPlayerScores();
		//deck->resetDeck();
		resetDeckAndRows();
		deal();
		printPlayerScores();
		printNumOfRounds();
		std::cout << "Round Finished" << std::endl;
	}
	//std::cout << "Deck Address: " << &deck << std::endl;
	printPlayerScoresOrdered();
	printNumOfRounds();
	//deck->printDeckSize();
}

int Game::pickRow(int playerIndex) {
	int myDiff;
	int diff=1000;
	int pickRow=-1;
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		if ( *rows[i] < *players[playerIndex] ) {
			myDiff = players[playerIndex]->faceOfChosenCard() - rows[i]->getTopCardFaceVal();
			if (myDiff < diff) {
				diff = myDiff;
				pickRow = i;
			}
		} else {
			break;
		}
	}
	return pickRow;
}

int Game::findSmallestPenRow(){
	int foundRow = 0;
	int rowPenVal;
	int penVal = 1000;
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		rowPenVal = penVal = rows[i]->getTotPenaltyVal();
		if ( penVal < rowPenVal ) {
			penVal = rowPenVal;
			foundRow = i;
		}
	}
	return foundRow;
}

void Game::placePlayerCards() {
	for ( int i = 0 ; i < _numberOfPlayers ; i++ ) {
		placePlayerCard(i);
	}
}

void Game::placePlayerCard(int playerIndex) {
	int row = pickRow(playerIndex);
	Card *playerCard = players[playerIndex]->placeCard();

	if ( row < 0 ) {
		// then no rows are eligible give player row w/ smallest penalty value
		row = findSmallestPenRow();
		players[playerIndex]->addRowPenalty(rows[row]->getTotPenaltyVal());
		//rows[row]->resetRow( players[playerIndex]->placeCard() );
		rows[row]->resetRow( playerCard );
		//std::cout << "No eligible Rows" << std::endl;
	} else if ( rows[row]->isFull() ) {
		// then row is full and you should give player row and replace with chosenCard
		players[playerIndex]->addRowPenalty(rows[row]->getTotPenaltyVal());
		//rows[row]->resetRow( players[playerIndex]->placeCard() );
		rows[row]->resetRow( playerCard );
	} else {
		//rows[row]->addCard(players[playerIndex]->placeCard());
		rows[row]->addCard( playerCard );
	}
	orderRows();
	deck->addCard( playerCard ); 	// put played card back into the deck, 
					// so that we don't run out of cards.

	//deck->addCard( players[playerIndex]->placeCard() );
	//std::cout << "****************" << std::endl;
	//printRows();
	//std::cout << "****************" << std::endl;
	//std::cout << "Deck Address: " << &deck << std::endl;
	//std::cout <<  std::endl;
	//deck->printDeck();

}

void Game::printRows() const {
	for ( int i = 0 ; i < _numberOfRows ; i++ ) {
		std::cout << i+1 << "st Row Face: \t" << rows[i]->getTopCardFaceVal() << 
			"\tTotal Pen: " << rows[i]->getTotPenaltyVal() << "\tRow Size: " 
			<< rows[i]->getNumberofCards() << std::endl;
	}
}

void Game::printPlayerScoresOrdered() {
	std::list<Player*> gamePlayers;
	for ( int i = 0; i < _numberOfPlayers; i++ ) {
		gamePlayers.push_front( players[i] );
	}
	gamePlayers.sort( comparePlayerScores );
	for( std::list<Player*>::iterator iter = gamePlayers.begin(); 
    		iter != gamePlayers.end(); iter++) {
		(*iter)->printScore();
	}
	gamePlayers.clear();
	//std::map<int, int> tempPlayers;
	//int playerNumber, playerScore;
	//for ( int i = 0; i < _numberOfPlayers ; i++ ) {
	//	playerNumber = players[i]->getPlayerNumber();
	//	playerScore = players[i]->getScore();
	//	tempPlayers[playerScore] = playerNumber;
	//}
	//for ( std::map<int, int>::iterator tp=tempPlayers.begin() ; tp != tempPlayers.end() ; tp++ ) {
	//	std::cout << "Player " << tp->second << "'s Score: " << (*tp).first << std::endl;
	//}
}

Random::Random( int initialValue ) {
	if ( initialValue < 0 ) {
		initialValue += M;
	}

	state = initialValue;
	if ( state == 0 ) {
		state = 1;
	}
}

int Random::randomInt() {
	int tmpState = A * ( state % Q ) - R * ( state / Q );

	if ( tmpState >= 0 ) {
		state = tmpState;
	} else {
		state = tmpState + M;
	}

	return state;
}

double Random::random0_1() {
	return (double) randomInt() / M;
}

void main() {
	FILE *stream ;
	
	//if (( stream = freopen("test0.txt", "w", stdout)) == NULL )
	//	exit(-1);

	/*
	std::ofstream randomNumbers("randomNums.txt", std::ios::app );
	if ( !randomNumbers.is_open() ) {
		std::cout << "Could not open random number output file." << std::endl;
		exit(-1);
	}
	*/

	std::srand((unsigned int) std::time(NULL));
	//std::cout << "Starting to Test Standard Functions\n" << std::endl;
	//Deck *d = new Deck;
	//delete d;
	//DeckQ *q = new DeckQ;
	//delete q;
	
	//for ( int i = 0; i < 10 ; i++ ) {
		Game *g = new Game;
		g->playGame();
		delete g;
		std::cout << "Game Finished!\n" << std::endl;
	//}

	std::cin.get();
}

/**
 * Construct the tree.
 * negInf is a value less than or equal to all others.
 */
//explicit RedBlackTree( const Comparable & negInf ) {
// template<class Comparable> 
// explicit RedBlackTree<Comparable>::RedBlackTree( const Comparable & negInf ) {
// 	nullNode = new RedBlackNode;
// 	nullNode->left = nullNode->right = nullNode;
// 	header = new RedBlackNode( negInf ) ;
// 	header->left = header->right = nullNode;
// }

// /**
//  * RedBlackTree Rotate Method
//  * Internal routine that performs a single or double rotation.
//  * Because the reult is attached to the parent, there are four cases.
//  * Called by handleReorient.
//  * item is the item in handleReorient.
//  * theParent is hte parent of the root of the rotated subtree.
//  * Return the root of the rotated subtree.
//  */
// RedBlackNode * rotate( const Comparable & item, RedBlackNode *theParent ) {
// 	if ( item < theParent->element ) {
// 		( item < theParent->left->element ) ? 
// 			rotateWithLeftChild( theParent->left ) : // LL
// 			rotateWithRightChild( theParent->left ); // LR
// 		return theParent->left;
// 	} else {
// 		( item < theParent->right->element ) ?
// 			rotateWithLeftChild( theParent->right ) : // RL
// 			rotateWithRightChild( theParent->right ); // RR
// 		return theParent->right;
// 	}
// }
// 
// /**
//  * RedBlackTree handleReorient method
//  * Internal routine that is called during an insertion if a node has two red
//  * children. Performs flip and rotations. item is the item being inserted.
//  */
// void handleReorient( const Comparable & item ) {
// 	// Do the color flip
// 	current->color = RED;
// 	current->left->color = BLACK;
// 	current->right->color = BLACK;
// 
// 	if ( parent->color == RED ) { // Have to rotate
// 		grand->color = RED;
// 		if ( ( item < grand->element ) != ( item < parent->element ) ) {
// 			parent = rotate( item, grand ); // Start dbl rotate
// 		}
// 		current = rotate( item, great );
// 		current->color = BLACK;
// 	}
// 	header->right->color = BLACK; // Make root black
// }
// 
// void insert( const Comparable & x ) {
// 	current = parent = grand = header;
// 	nullNode->element = x;
// 
// 	while ( current->element != x ) {
// 		great = grand;
// 		grand = parent;
// 		parent = current;
// 		current = ( x < current->element ) ? current->left : current->right;
// 		// Check if two red children; fix if so
// 		if ( current->left->color == RED && current->right->color == RED ) {
// 			handleReorient( x );
// 		}
// 	}
// 
// 	// Insertion fails if already present
// 	if ( current != nullNode ) {
// 		return;
// 	}
// 	current = new RedBlackNode( x, nullNode, nullNode );
// 
// 	// Attach to parent
// 	if ( x < parent->element ) {
// 		parent->left = current;
// 	} else {
// 		parent->right = current;
// 	}
// 	handleReorient( x );
// }
// 
// void printTree() const {
// 	if ( header->right == nullNode ) {
// 		std::cout << "Empty tree" << std::endl;
// 	} else {
// 		printTree( header->right );
// 	}
// }
// 
// void printTree( RedBlackNode *t ) const {
// 	if ( t != t->left ) {
// 		printTree( t->left );
// 		std::cout << t->element << std::endl;
// 		printTree( t->right );
// 	}
// }
// 
// const RedBlackTree & operator=( const RedBlackTree & rhs ) {
// 	if ( this != & rhs ) {
// 		makeEmpty();
// 		header->right = clone( rhs.header->right );
// 	}
// 
// 	return *this;
// }
// 
// const RedBlackNode * clone( RedBlackNode * t ) const {
// 	if ( t == t->left ) { // Cannot test agains nullNode!!
// 		return nullNode;
// 	} else {
// 		return new RedBlackNode( t->element, clone( t->left ),
// 				clone( t->right ), t->color );
// 	}
// }

/*
 * Game Class Helper Function - Private
 * Name: playerQuickSort
 * 
 * Function takes two arguments: index at the back&front of the array.
 * Then the quick sort algorithm is applied to the array. First checks if
 * the contents of the *left are greater than the *middle, if not move on until
 * a greater value is found. Then work from the right to the middle. Comparing
 * *right to *middle until a value less than *middle is found. When the two numbers
 * are found swaps left and right index values and recurse until all numbers on left
 * are less than middle, and all nmbers on the right are greater than middle.
**/
/*
void Game::playerQuickSort(int left, int right) {
	int arraySize = (right - left) + 1;
	if( (arraySize > 1) && (left < right) ) {
		//int middle = right/2; // Seems to be more efficient if we go from end to end
		int middle = right;
		int intLeft = left;
		int intRight = right;
		while ( left != right ) {
			//std::cout << "Compare left to middle " << std::endl;
			while ( *players[left] < *players[middle] ) {
				//std::cout << players[left]->faceOfChosenCard() << " < " << 
					//players[middle]->faceOfChosenCard() << std::endl;
				left++;
			}
			//std::cout << "Compare right to middle " << std::endl;
			while ( *players[right] > *players[middle] ) {
				//std::cout << players[right]->faceOfChosenCard() << " > " << 
					//players[middle]->faceOfChosenCard() << std::endl;
				right--;
			}
			//std::cout << "left, right middle " << left << "," << right << "," << middle << std::endl;
			if ( left == middle ) {
				middle = right;
			}
			else if ( right == middle ) {
				middle = left;
			}
			//std::cout << "left, right middle " << left << "," << right << "," << middle << std::endl;
			Player *temp = players[left];
			players[left] = players[right];
			players[right] = temp;
			if ( ( *players[left] == *players[middle] ) || ( *players[right] == *players[middle] )) {
				break;
			}
		}
		playerQuickSort(intLeft, middle-1);
		playerQuickSort(middle+1, intRight);
	}
	
}
*/

/*
void Game::rowQuickSort(int left, int right) {
	int arraySize = (right - left) + 1;
	if( (arraySize > 1) && (left < right) ) {
		int middle = right;
		int intLeft = left;
		int intRight = right;
		while ( left != right ) {
			// Find left row whose face is small than middle
			while ( *rows[left] < *rows[middle] ) {
				left++;
			}
			// Find right row whose face is larger than middle
			while ( *rows[right] > *rows[middle] ) {
				right--;
			}
			if ( left == middle ) {
				middle = right;
			}
			else if ( right == middle ) {
				middle = left;
			}
			Row *temp = rows[left];
			rows[left] = rows[right];
			rows[right] = temp;
			if ( ( *rows[left] == *rows[middle] ) || ( *rows[right] == *rows[middle] )) {
				break;
			}
		}
		rowQuickSort(intLeft, middle-1);
		rowQuickSort(middle+1, intRight);
	}
}
*/

/*
 * Row Class Helper Function - Public
 * Name: createNewRow
 * takes a card
 * resets all of the row's values to zero
 * adds that new card to the row
**/
/*
void Row::createNewRow(Card & c) {
	this->_numberofCards = 0;
	this->_topCardFaceVal = 0;
	this->_totPenaltyVal = 0;
	this->addCard(c);
}
*/

/*
template<class T>
class CursorBasedQueueCircular {
	private:
		int _size;
		int front, back;
		T *queue;
	public:
		CursorBasedQueueCircular(int size = 104 );
		void enqueue(T item);
		T dequeue();
		void printContents();
		  
};

// 	for(std::vector<Card*>::iterator iter = playerCards.begin(); iter != playerCards.end(); iter++) {
// 		for ( int i = 0; i < numberOfRows ; i++ ){
// 			if ( isRowEligible( (*iter), gameRows[i] ) ) {
// 				if ( i == 0 ) {
// 					rowOneElg[ (*iter) ] = (*iter)->getFaceVal() - gameRows[i]->getTopCardFaceVal();
// 				} else if ( i == 1 ) {
// 					rowTwoElg[ (*iter) ] = (*iter)->getFaceVal() - gameRows[i]->getTopCardFaceVal();
// 				} else if ( i == 2 ) {
// 					rowThrElg[ (*iter) ] = (*iter)->getFaceVal() - gameRows[i]->getTopCardFaceVal();
// 				} else if ( i == 3 ) {
// 					rowFouElg[ (*iter) ] = (*iter)->getFaceVal() - gameRows[i]->getTopCardFaceVal();
// 				} else {
// 					std::cout << "How can this be? No one has every made it here..." << std::endl;
// 				}
// 			} else {
// 				uneligible.push_back( (*iter) );
// 			}
// 	}
// 	std::cout << "Row One Elibible" << std::endl;
// 	gameRows[0]->printRowStatus();
// 	printEligibleElementsInRow( rowOneElg );
// 	std::cout << "Row Two Elibible" << std::endl;
// 	gameRows[1]->printRowStatus();
// 	printEligibleElementsInRow( rowTwoElg );
// 	std::cout << "Row Three Elibible" << std::endl;
// 	gameRows[2]->printRowStatus();
// 	printEligibleElementsInRow( rowThrElg );
// 	std::cout << "Row Four Elibible" << std::endl;
// 	gameRows[3]->printRowStatus();
// 	printEligibleElementsInRow( rowFouElg );
// 	//std::cout<< (*iter)->getFaceVal() << std::endl;
// 	//
// 
// 	// Find Row Smallest Difference
// 
// 	int smallestPen = 1000;
// 	int rowWithSmallestPen = -1;
// 	std::map<Card*, int>::iterator iter;
// 	// Find row w/ Elibile Card having smallest pen
// 	//for ( int i = 0 ; i < numberOfRows ; i++ ){
// 	//}
// 	if ( !rowOneElg.empty() ) {
// 		std::cout << "Row One has eligible cards" << std::endl;
// 		if ( gameRows[0]->getTotPenaltyVal() < smallestPen ) {
// 			smallestPen = gameRows[0]->getTotPenaltyVal();
// 			rowWithSmallestPen = 0;
// 		}
// 	}
// 	if ( !rowTwoElg.empty() ) {
// 		std::cout << "Row Two has eligible cards" << std::endl;
// 		if ( gameRows[1]->getTotPenaltyVal() < smallestPen ) {
// 			smallestPen = gameRows[1]->getTotPenaltyVal();
// 			rowWithSmallestPen = 1;
// 		}
// 	}
// 	if ( !rowThrElg.empty() ) {
// 		std::cout << "Row Three has eligible cards" << std::endl;
// 		if ( gameRows[2]->getTotPenaltyVal() < smallestPen ) {
// 			smallestPen = gameRows[2]->getTotPenaltyVal();
// 			rowWithSmallestPen = 2;
// 		}
// 	}
// 	if ( !rowFouElg.empty() ) {
// 		std::cout << "Row Four has eligible cards" << std::endl;
// 		if ( gameRows[2]->getTotPenaltyVal() < smallestPen ) {
// 			smallestPen = gameRows[2]->getTotPenaltyVal();
// 			rowWithSmallestPen = 3;
// 		}
// 	}
// 
// 	if ( rowWithSmallestPen == -1 ) {
// 		std::cout << "No compatible cards, for rows." << std::endl;
// 	} else {
// 		std::cout << "Row chosen to have the smallest penalty with the compatible cards is: " << rowWithSmallestPen << std::endl;
// 	}
// 	// Check first row
// 	iter = rowOneElg.begin();
// 	if ( iter->second < smallestDifference ) {
// 		smallestDifference = iter->second;
// 		rowWithSmallestDiff = 0;
// 	}
// 	// Check Second Row
// 	iter = rowTwoElg.begin();
// 	if ( iter->second < smallestDifference ) {
// 		smallestDifference = iter->second;
// 		rowWithSmallestDiff = 1;
// 	}
// 	// Check Third Row
// 	iter = rowThrElg.begin();
// 	if ( iter->second < smallestDifference ) {
// 		smallestDifference = iter->second;
// 		rowWithSmallestDiff = 2;
// 	}
// 	// Check Fourth Row
// 	iter = rowFouElg.begin();
// 	if ( iter->second < smallestDifference ) {
// 		smallestDifference = iter->second;
// 		rowWithSmallestDiff = 3;
// 	}
*/
//
// 		std::cout << "Start Evaluating Card Against Row: " << (*iter)->getFaceVal() << " Index " << currentIndex << std::endl;
// 		difference = (*iter)->getFaceVal() - gameRow[currentIndex]->getTopCardFaceVal() ;
// 		while ( difference > 0 ) {
// 			newIndex = currentIndex + 1;
// 			if ( ( currentIndex >= numberOfRows ) || ( newIndex >= numberOfRows ) ) { 
// 				break;
// 			}
// 
// 			newDifference = (*iter)->getFaceVal() - gameRow[newIndex]->getTopCardFaceVal() ;
// 			if ( newDifference <= 0 ) {
// 				//std::cout << "****************************************************************" << std::endl;
// 				//std::cout << "****************************************************************" << std::endl;
// 				//std::cout << "Next Row is greater than current card." << std::endl;
// 				//std::cout << "Row, Card " << gameRow[newIndex]->getTopCardFaceVal() << ", " << (*iter)->getFaceVal() << std::endl;
// 				//std::cout << "Choosing Row " << currentIndex << " with card " << gameRow[currentIndex]->getTopCardFaceVal() << ", " << (*iter)->getFaceVal() << std::endl;
// 				difference = newDifference;
// 				//std::cout << "****************************************************************" << std::endl;
// 				//std::cout << "****************************************************************" << std::endl;
// 			} else {
// 				//std::cout << "################################################################" << std::endl;
// 				//std::cout << "################################################################" << std::endl;
// 				//std::cout << "Next Row is less than current card, increment row index." << std::endl;
// 				//std::cout << "Row, Card " << gameRow[newIndex]->getTopCardFaceVal() << ", " << (*iter)->getFaceVal() << std::endl;
// 				currentIndex = newIndex;
// 				//difference = newDifference;
// 				//std::cout << "################################################################" << std::endl;
// 				//std::cout << "################################################################" << std::endl;
// 			}
// 		}
// 		std::cout << "Choosing Row " << currentIndex << " with card " << gameRow[currentIndex]->getTopCardFaceVal() << ", Card Compared: " << (*iter)->getFaceVal() << std::endl;
// 		std::cout << "Difference " << difference << std::endl << std::endl;
		//std::cout << "Choosing Row " << currentIndex << " with card " << gameRow[currentIndex]->getTopCardFaceVal() << ", Pick Card " << (*iter)->getFaceVal() << " Difference " << newDifference << std::endl;
		//index = checkSmallestDifference(difference, index, (*iter)->getFaceVal(), gameRow);
		//while (difference > 0) {
		//	if ( index >= numberOfRows ) {
		//		break;
		//	}
		//	index++;
		//	std::cout << "Difference is greater than zero..." << std::endl;
		//	difference = (*iter)->getFaceVal() - gameRow[index]->getTopCardFaceVal() ;
		//}
		//if ( difference > 0 ) {
		//	std::cout << "Eligible Card Found for row " << index << std::endl;
		//	//eligibleCards[ gameRow[index] ] = *iter;
		//}
		//if ( difference > 0 ) {
		//	std::cout << " Card is eligible with row " << i << " " << (*iter)->getFaceVal() << std::endl;
		//	i++;
		//	if ( i >= numberOfRows ) {
		//		break;
		//	}
		//}
//	}
template<class Comparable>
class RedBlackTree {
	private:
		struct RedBlackNode {
			Comparable element;
			RedBlackNode *left;
			RedBlackNode *right;
			int color;

			RedBlackNode( const Comparable & theElement = Comparable(), 
				RedBlackNode *lt = NULL, RedBlackNode *rt = NULL, int c = BLACK ) : 
				element( theElement ), left( lt ), right( rt ), color( c ) {}
		};

		RedBlackNode *header; // the tree header (contains negInf)
		RedBlackNode *nullNode;

		RedBlackNode *current;
		RedBlackNode *parent;
		RedBlackNode *grand;
		RedBlackNode *great;

		void relaimMemory( RedBlackNode *t );
		void printTree( RedBlackNode *t ) const {
			if ( t != t->left ) {
				printTree( t->left );
				std::cout << t->element << std::endl;
				printTree( t->right );
			}
		};

		//RedBlackNode * clone( RedBlackNode * t ) const;
		RedBlackNode * clone( RedBlackNode * t ) const {
			if ( t == t->left ) { // Cannot test agains nullNode!!
				return nullNode;
			} else {
				return new RedBlackNode( t->element, clone( t->left ),
						clone( t->right ), t->color );
			}
		};

		void handleReorient( const Comparable & item ) {
			// Do the color flip
			current->color = RED;
			current->left->color = BLACK;
			current->right->color = BLACK;
		
			if ( parent->color == RED ) { // Have to rotate
				grand->color = RED;
				if ( ( item < grand->element ) != ( item < parent->element ) ) {
					parent = rotate( item, grand ); // Start dbl rotate
				}
				current = rotate( item, great );
				current->color = BLACK;
			}
			header->right->color = BLACK; // Make root black
		};

		RedBlackNode * rotate( const Comparable & item, RedBlackNode *theParent ) {
			if ( item < theParent->element ) {
				( item < theParent->left->element ) ? 
					rotateWithLeftChild( theParent->left ) : // LL
					rotateWithRightChild( theParent->left ); // LR
				return theParent->left;
			} else {
				( item < theParent->right->element ) ?
					rotateWithLeftChild( theParent->right ) : // RL
					rotateWithRightChild( theParent->right ); // RR
				return theParent->right;
			}
		};

		void rotateWithLeftChild( RedBlackNode * & k2 );
		void rotateWithRightChild( RedBlackNode * & k1 );
	
	public:
		explicit RedBlackTree( const Comparable & negInf ) { 
			nullNode = new RedBlackNode;
			nullNode->left = nullNode->right = nullNode;
			header = new RedBlackNode( negInf ) ;
			header->left = header->right = nullNode;
		};
		//RedBlackTree( const Comparable & negInf );
		RedBlackTree( const RedBlackTree & rhs );
		~RedBlackTree();

		const Comparable & findMin() const;
		const Comparable & findMax() const;
		bool contains( const Comparable & x ) const;
		bool isEmpty() const;
		void printTree() const {
			if ( header->right == nullNode ) {
				std::cout << "Empty tree" << std::endl;
			} else {
				printTree( header->right );
			}
		};

		void makeEmpty();
		void insert( const Comparable & x ) {
			current = parent = grand = header;
			nullNode->element = x;
		
			while ( current->element != x ) {
				great = grand;
				grand = parent;
				parent = current;
				current = ( x < current->element ) ? current->left : current->right;
				// Check if two red children; fix if so
				if ( current->left->color == RED && current->right->color == RED ) {
					handleReorient( x );
				}
			}
		
			// Insertion fails if already present
			if ( current != nullNode ) {
				return;
			}
			current = new RedBlackNode( x, nullNode, nullNode );
		
			// Attach to parent
			if ( x < parent->element ) {
				parent->left = current;
			} else {
				parent->right = current;
			}
			handleReorient( x );
		};

		void remove( const Comparable & x );

		enum { RED, BLACK };

		const RedBlackTree & operator=( const RedBlackTree & rhs ) {
			if ( this != & rhs ) {
				makeEmpty();
				header->right = clone( rhs.header->right );
			}
		
			return *this;
		};
};
