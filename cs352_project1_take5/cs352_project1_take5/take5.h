#include <vector>
#include <list>
#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <map>

#ifndef Random_h
#define Random_h
/**
 * Random from Data Structure Algorithm and Analysis in C++ by Mark Weiss
 * Chapter 10 Algorithm Design Techniques - Randomized Algorithms p. 457
 * Tested and didn't get very good results; sticking with the standard library;
 */
class Random {
	private:
		static const int A = 48271;
		static const int M = 2147483647;
		static const int Q = M / A;
		static const int R = M % A;
		int state;
	public:
		explicit Random( int initialValue = 1 );
		int randomInt();
		double random0_1();

};

#endif // Random_h

#ifndef BinarySearchTree_h
#define BinarySearchTree_h

template <typename Comparable>
class BinarySearchTree {
	private:
		struct BinaryNode {
			Comparable element;
			BinaryNode *left;
			BinaryNode *right;

			BinaryNode( const Comparable & theElement, BinaryNode *lt, BinaryNode *rt ) : 
				element( theElement ), left( lt ), right( rt ) {}
		};

		BinaryNode *root;

		void insert( const Comparable & x, BinaryNode * & t ) const {
			if ( t == NULL ) {
				t = new BinaryNode( x, NULL, NULL );
			} else if ( x < t->element ) {
				insert( x, t->left );
			} else if ( t->element < x ) {
				insert( x, t->right );
			} else {
				;
			}
		};

		void remove( const Comparable & x, BinaryNode * & t ) const {
			if ( t == NULL ) {
				return;
			}
			if ( x < t->element ) {
				remove( x, t->left );
			} else if ( t->element < x ) {
				remove( x, t->right );
			} else if ( t->left != NULL && t->right != NULL ) {
				t->element = findMin( t->right )->element;
				remove( t->element, t->right );
			} else {
				BinaryNode *oldNode = t;
				t = ( t->left != NULL ) ? t->left : t->right;
				delete oldNode;
			}
		};

		BinaryNode * findMin( BinaryNode *t ) const {
			if ( t == NULL )
				return NULL;
			if ( t->left == NULL )
				return t;
			return findMin( t->left );
		};

		BinaryNode * findMax( BinaryNode *t ) const {
			if ( t != NULL ) {
				while ( t->right != NULL ) {
					t = t->right;
				}
			}
			return t;
		};

		bool contains( const Comparable & x, BinaryNode *t ) const {
			if ( t == NULL )
				return false;
			else if ( x < t->element )
				return contains( x, t->left );
			else if ( t->element < x )
				return contains( x, t->right );
			else
				return true;
		};

		void makeEmpty( BinaryNode * & t ) {
			if ( t != NULL ) {
				makeEmpty( t->left );
				makeEmpty( t->right );
				delete t;
			}
			t = NULL;
		};

		void printTree( BinaryNode *t ) const;
		BinaryNode * clone( BinaryNode *t ) const {
			if ( t == NULL ) {
				return NULL;
			}

			return new BinaryNode( t->element, clone( t->left ), clone( t->right ) );
		};

	public:
		BinarySearchTree() {
			root = NULL;
		};
		//BinarySearchTree( const BinarySearchTree & rhs );
		~BinarySearchTree() {
			makeEmpty();
		};

		const Comparable & findMin() const {
			findMin( root );
		};

		const Comparable & findMax() const {
			findMax( root );
		};

		bool contains( const Comparable & x ) const {
			return contains( x, root);
		};

		bool isEmpty() const {
			return ( root == NULL ) ? true : false;
		};
		void printTree() const;

		void makeEmpty() {
			makeEmpty( root );
		};
		void insert( const Comparable & x ) {
			insert( x, root);
		};
		void remove( const Comparable & x ) {
			remove( x, root );
		};

		const BinarySearchTree & operator=( const BinarySearchTree & rhs ) {
			if ( this != &rhs ) {
				makeEmpty();
				root = clone ( rhs.root );
			}
			return *this;
		};
};
#endif // BinarySearchTree_h

#ifndef Card_h
#define Card_h

class Card {
	private: 
		int _faceVal;
		int _penaltyVal;

		void setFaceVal(int face);
		void setPenaltyVal(int penVal);

	public:
		Card(int face = 0, int penalty = 0);
		~Card();
		bool operator< (const Card & rhs) const;
		bool operator> (const Card & rhs) const;
		bool operator== (const Card & rhs) const;
		int getFaceVal() const;
		int getPenaltyVal() const;
};

#endif // Card_h

#ifndef Deck_h
#define Deck_h

class Deck {
	private:
		static const int _fullDeckSize = 104;
		int _currentSize; // Used as an index variable for the stack
		Card *deck[_fullDeckSize];

		void shuffle();
		void newDeck();
		void addCard(Card *newCard);
		void resetSize();

	public:
		Deck();
		~Deck();

		bool isEmpty() const;
		void resetDeck();
		Card* dealCard();
		int numberRemaining() const;
		void printDeck();
		void printDeckSize() const;
		
};

class DeckQ {
	private:
		static const int _fullDeckSize = 104;
		int _front, _back; // Used as index variables for the queue
		Card *deck[_fullDeckSize];

		void shuffle();
		void newDeck();
		void resetSize();

		Random rand;


	public:
		DeckQ();
		~DeckQ();

		void addCard(Card *newCard);
		bool isEmpty() const;
		void resetDeck();
		Card* dealCard();
		int numberRemaining() const;
		void printDeck();
		void printDeckSize() const;
		
};

#endif // Deck_h


#ifndef Player_h
#define Player_h

class Row;
class Player {
	private:
		static const int _maxHandSize=10;
		int _numberOfCards;
		int _penaltyPileScore;
		//enum  StrategyType _playerStrategy;
		//int _playerStrategy;
		int _playerNumber;
		bool _isDeckSorted;
		Card *hand[_maxHandSize];
		Card *playerChosenCard;
		//int indexPlayerChosenCard;

		//void smartPlayer();
		void smartPlayer(Row** rows);
		void sortPlayerHand( );

		void sortHandLargestToSmallest();
		void sortHandSmallestToLargest();

		void associateCardWithRow(Row** gameRow, std::vector<Card*> &myHand,std::vector<Card*> &notEligible, std::map<Card*,Row*> &eligibleCards);
		int checkSmallestDifference(int currDiff, int currRow, int cardFace, Row** rows);
		int findSmallestRowPenalty(Row** gameRow);
	
	public:
		enum StrategyType { LargestToSmallest, SmallestToLargest, SmartPlayerSmall, SmartPlayerLarge, Random } _playerStrategy;
		Player();
		Player(int num);
		Player(int num, StrategyType strategy);
		~Player();

		Card *placeCard();


		bool operator< (const Player & rhs);
		bool operator> (const Player & rhs);
		bool operator> (const int rhs);
		bool operator== (const Player & rhs);
		int numberOfCardsHeld() const;
		int getPlayerNumber() const;
		bool isEmptyHand() const;
		int faceOfChosenCard() const;
		void addCard(Card *newCard);
		void addRowPenalty(int pen);
		void printPlayerHand() const;
		int getScore() const;
		void printScore() const;

		//void pickCard();
		void pickCard(Row** gameRows);


};

class Player1 : public Player {
	private:
		static const int _maxHandSize=10;
		/*
		struct HandNode {
			Card *card;
			Card *nextCard;
			Card *previousCard;
		};
		*/
		int _numberOfCards;
		int _penaltyPileScore;
		int _playerStrategy;
		int _playerNumber;
		//std::vector<Card*> hand[_maxHandSize];
		BinarySearchTree<Card*> playerCards;
		Card *playerChosenCard;

		void pickCard(int playerStrategy);
	
	public:
		Player1(int num);
		~Player1();

		Card *placeCard();

		//bool operator< (const Player & rhs);
		//bool operator> (const Player & rhs);
		//bool operator> (const int rhs);
		//bool operator== (const Player & rhs);

		//int numberOfCardsHeld() const;
		//int getPlayerNumber() const;
		//bool isEmptyHand() const;
		//int faceOfChosenCard() const;
		//void addRowPenalty(int pen);
		//int getScore() const;
		//void printScore() const;

		void addCard(Card *newCard);
		void printPlayerHand() const;
		void pickCard();
};

#endif //Player_h

#ifndef Row_h
#define Row_h

class Player;
class Row {
	private:
		static const int _fullRow=5;

		int _totPenaltyVal;
		int _topCardFaceVal;
		int _numberofCards;
		
		//void createNewRow(Card & c);
		void addPenalty(int addVal);

	public:
		Row();
		Row(Card * c);

		bool operator< (const Row & rhs);
		bool operator< (const Player & rhs);
		bool operator> (const Row & rhs);
		bool operator> ( const Player & rhs );
		bool operator== (const Row & rhs);
		int getNumberofCards() const;
		int getTotPenaltyVal() const;
		int getTopCardFaceVal() const;

		void addCard(Card * c);
		void resetRow(Card * c);
		bool isFull() const;

		void printRowStatus() const;

};
#endif // Row_h

#ifndef Game_h
#define Game_h

class Game {
	private:
		static const int _numberOfRows=4;
		static const int _maxNumberOfPlayers=4;
		static const int _maxPlayerScore=94;

		int _numberOfPlayers;
		int _numberOfRounds;

		int nextPlayerIndex;
		
		DeckQ *deck;
		Player *players[_maxNumberOfPlayers];
		Row *rows[_numberOfRows];

		bool endOfGame() const;	
		bool playerHandsEmpty() const;

		int findSmallestPenRow();
		int pickRow(int playerIndex);

		void deal();
		void orderPlayers();
		void orderRows();
		void placePlayerCard(int playerIndex);
		void placePlayerCards();
		void playersPickCards();
		void playRound();
		void resetDeckAndRows();

		//template<class T> void quickSort(T** thing, int top, int bottom);

		//void playerQuickSort(int top, int bottom);
		//void rowQuickSort(int top, int bottom);
		//void setNumberOfPlayers(int numOfPlayers);
		//void startGame(int numOfPlayers);
		//void createPlayer();

	public:
		Game(int numOfPlayers = 4);
		~Game();
		
		void playGame();
		void printNumOfRounds() const;
		void printPlayerHands() const;
		void printPlayerScores() const;
		void printPlayerScoresOrdered();
		void printRows() const;
};

#endif // Game_h


#ifndef General_h
#define General_h

template<class T> void quickSort(T** thing, int left, int right) {
	int thingSize = (right - left) + 1;
	if( (thingSize > 1) && (left < right) ) {
		int middle = right;
		int intLeft = left;
		int intRight = right;
		while ( left != right ) {
			// Find left row whose face is small than middle
			while ( *thing[left] < *thing[middle] ) {
				left++;
			}
			// Find right row whose face is larger than middle
			while ( *thing[right] > *thing[middle] ) {
				right--;
			}
			if ( left == middle ) {
				middle = right;
			}
			else if ( right == middle ) {
				middle = left;
			}
			T *temp = thing[left];
			thing[left] = thing[right];
			thing[right] = temp;
			if ( ( *thing[left] == *thing[middle] ) || ( *thing[right] == *thing[middle] )) {
				break;
			}
		}
		quickSort(thing, intLeft, middle-1);
		quickSort(thing, middle+1, intRight);
	}
};

// isCardGreater Function for vector sort functor
bool isCardGreater( Card * lhs, Card * rhs ) {
	return lhs->getFaceVal() > rhs->getFaceVal();
};

bool isRowEligible( Card * lhs, Row * rhs ) {
	return ( ( lhs->getFaceVal() - rhs->getTopCardFaceVal() )  > 0 ) ? true : false;
};

bool comparePlayerScores( Player *lhs, Player *rhs ) {
	return lhs->getScore() < rhs->getScore();
};

// void printEligibleElementsInRow(std::map<Card*, int> row) {
// 	if ( row.empty() ) {
// 		return;
// 	}
// 	for ( std::map<Card*, int>::iterator iter = row.begin(); iter != row.end(); iter++ ) {
// 		std::cout << "Card Face: " << iter->first->getFaceVal() << " Row Difference: " << iter->second << std::endl;
// 	}
// }

#endif // General_h
